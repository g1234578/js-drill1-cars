const {inventory} = require('../JSDrill1-Cars/inventory.js');

// ==== Problem #4 ====
// The accounting team needs all the years from every car on the lot. Execute a function that will return an array from the dealer data containing only the car years and log the result in the console as it was returned.

function getCarYears(inventory){
    if(Array.isArray(inventory) ){
        let carYears = [];

        for(let index=0;index<inventory.length;index++){
           carYears.push(inventory[i].car_year);
        }

        return carYears;
    }

    else{
        return [];
    }
    
}

module.exports = getCarYears;