// ==== Problem #1 ====
// The dealer can't recall the information for a car with an id of 33 on his lot. 
//Help the dealer find out which car has an id of 33 by calling a function that will return the data for that car. 
//Then log the car's year, make, and model in the console log in the format of:

function findCarById(inventory,id) {
    let car;
    if(Array.isArray(inventory) && id >=1 && id <= 50){
        for (let index = 0; index < inventory.length; index++) {
            if (inventory[index].id === id) {
                 car= inventory[index];
            }
        }
        return car;
    }
    else {
        return [];
    }
}

module.exports = findCarById;
