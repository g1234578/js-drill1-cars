
const {inventory} = require('../../JSDRILL1-CARS/inventory.js');
const findCarById = require('../../JSDRILL1-CARS/problem1.js');


let car = findCarById(inventory,33);

if(car != ""){
   console.log(`Car ${car.id} is a ${car.car_year} ${car.car_make} ${car.car_model}`);
}

else{
    console.log("Please check again");
}