const {inventory} = require('../../JSDRILL1-CARS/inventory.js');
const lastCarInInventory = require('../../JSDRILL1-CARS/problem2.js');

const lastCar = lastCarInInventory(inventory);

if(lastCar != ""){
    console.log(`Last car is a ${lastCar.car_make} ${lastCar.car_model}`);
}

else{
    console.log("Check again");
}
